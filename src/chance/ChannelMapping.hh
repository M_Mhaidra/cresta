#ifndef __CHANNEL_MAPPING_HH__
#define __CHANNEL_MAPPING_HH__


enum CHANNELMAP {
	kRPCX = 1,
	kRPCY,
	kDriftX,
	kDriftY,
	kRPCAboveX,
	kRPCAboveY,
	kRPCBelowX,
	kRPCBelowY,
	kDriftAboveX,
	kDriftAboveY,
	kDriftBelowX,
	kDriftBelowY,
	kAboveX,
	kAboveY,
	kBelowX,
	kBelowY,
	kAllX,
	kAllY,
	kAbove,
	kBelow
};

#endif