#ifndef __EventVertex_HH__
#define __EventVertex_HH__


struct EventTracks {
  float x;
  float y;
  float z;
  float px1;
  float py1;
  float px2;
  float py2;
  float thx;
  float thy;
  float th;
  float mom;
};


struct EventVertex {
  float x;
  float y;
  float z;
  float thx;
  float thy;
  float th;
  float mom;
};

#endif