{
  name: "DISCRIMINATOR"
  index: "config",
  grid_sizex: 10.0,	
  grid_minx: -1000.0
  grid_maxx: 1000.0
  grid_sizey: 10.0
  grid_miny: -1000.0
  grid_maxy: 1000.0
  grid_sizez: 10.0
  grid_minz: -500.0
  grid_maxz: 500.0
  point_offsetx: -250.0
  point_offsety: -250.0
  point_offsetz: 0.0
  grid_offsetx: 0.0
  grid_offsety: 0.0
  grid_offsetz: 0.0
  smearing: 0.0
  force_momentum: 0.0
  smear_momentum: 0.50
}